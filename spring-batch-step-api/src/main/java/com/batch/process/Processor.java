package com.batch.process;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.batch.entity.User;

@Component
public class Processor implements ItemProcessor<User, User>{

	public static Map<String,String> DEPT_MAPPER = new HashMap<>();
	
	public Processor() {
		DEPT_MAPPER.put("001","Technology");
		DEPT_MAPPER.put("002","Account");
		DEPT_MAPPER.put("003","Operation");
		DEPT_MAPPER.put("004","Security");
	}
	@Override
	public User process(User user) throws Exception {
		
		String deptCode = user.getDept();
		user.setDept(DEPT_MAPPER.get(deptCode));
		return user;
	}

}
