package com.onetomany.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onetomany.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>{
	
	//alternate for join columns query.Spring jpa provided. 
	//findByClassNameFieldName. example given below
	public List<Student> findByClassRoomId(int classId);

}
