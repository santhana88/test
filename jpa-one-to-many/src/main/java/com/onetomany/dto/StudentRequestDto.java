package com.onetomany.dto;

import java.util.List;

import com.onetomany.entity.ClassRoom;
import com.onetomany.entity.Student;

public class StudentRequestDto {
	
	private ClassRoom classRoom;
	
	private List<Student> students;

	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

}
