package com.onetomany.dto;

import com.onetomany.entity.ClassRoom;

public class ClassRoomRequestDto {
	
	private ClassRoom classRoom;

	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}
	

}
