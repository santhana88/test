package com.onetomany.dto;

import java.util.List;

import com.onetomany.entity.ClassRoom;
import com.onetomany.entity.Student;

public class ResponseDto {
	
	private ClassRoom classRoom;
	
	private List<Student> students;
	private String status;
	private String description;
	
	public ResponseDto(ClassRoom classRoom, List<Student> students, 
			String status, String description) {
		this.classRoom=classRoom;
		this.students=students;
		this.status = status;
		this.description=description;
		
	}

	public String getStatus() {
		return status;
	}

	public String getDescription() {
		return description;
	}

	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public List<Student> getStudents() {
		return students;
	}

}
