package com.onetomany.dto;


import java.util.List;

import com.onetomany.entity.ClassRoom;

public class ClassRoomResponseDto {
	
	private ClassRoom classRoom;
	private String status;
	private String description;
	private List<ClassRoom> classRooms;
	
	public ClassRoomResponseDto(ClassRoom classRoom, 
		String status, String description) {
	this.classRoom=classRoom;
	this.status = status;
	this.description=description;
		
	}
	
	public ClassRoomResponseDto(String status, String description,List<ClassRoom> classRoom) {
		this.classRooms=classRoom;
		this.status = status;
		this.description=description;
			
		}
		
	
	public List<ClassRoom> getClassRooms() {
		return classRooms;
	}

	public String getStatus() {
		return status;
	}

	public String getDescription() {
		return description;
	}

	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}
	

}
