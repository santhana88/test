package com.onetomany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onetomany.dto.ClassRoomRequestDto;
import com.onetomany.dto.ClassRoomResponseDto;
import com.onetomany.service.ClassRoomService;

@RestController
@RequestMapping("/api/classroom")
public class ClassRoomController {
	
	@Autowired
	private ClassRoomService service;
	
	@PostMapping("/")
	public ClassRoomResponseDto saveClassRoom(@RequestBody ClassRoomRequestDto classRoom) {
		
		return service.saveClassRoom(classRoom);
		
	}
	
	@GetMapping("/{id}")
	public ClassRoomResponseDto getClassRoomDetails(@PathVariable("id") int classId) {
		return service.getClassRoomDetails(classId);
	}
	
	@GetMapping("/allClass")
	public ClassRoomResponseDto getAllClassRoomDetails() {
		return service.getAllClassRoomDetails();
	}

}
