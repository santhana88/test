package com.onetomany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onetomany.dto.ResponseDto;
import com.onetomany.dto.StudentRequestDto;
import com.onetomany.service.StudentService;

@RestController
@RequestMapping("/api/student")
public class StudentController {
	
	@Autowired
	private StudentService service;
	
	@PostMapping("/")
	public ResponseDto saveStudentsWithClass(@RequestBody StudentRequestDto studentDto) {
		return service.saveStudents(studentDto);
	}
	
	@GetMapping("/{id}")
	public ResponseDto getStudentDetails(@PathVariable("id") int studentId) {
		return service.getStudentDetails(studentId);
	}
	
	@GetMapping("/classId/{id}")
	public ResponseDto getStudentDetailsByClass(@PathVariable("id") int classId) {
		return service.getStudentsDetailsbyClass(classId);
	}

}
