package com.onetomany.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.onetomany.dto.ResponseDto;
import com.onetomany.dto.StudentRequestDto;
import com.onetomany.entity.ClassRoom;
import com.onetomany.entity.Student;
import com.onetomany.repository.ClassRoomRepository;
import com.onetomany.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private ClassRoomRepository classRepository;
	
	@Autowired
	private StudentRepository repository;
	
	@Transactional(propagation = Propagation.REQUIRED, isolation =  Isolation.SERIALIZABLE, 
			readOnly = false)
	public ResponseDto saveStudents(StudentRequestDto studentDto) {
		List<Student> students = new ArrayList<Student>();
		Optional<ClassRoom>classroom = classRepository
				.findById(studentDto.getClassRoom().getId());
		for(Student student:studentDto.getStudents()) {
			student.setClassRoom(classroom.get());
			students.add(repository.save(student));
		}
		
		/*
		 * studentDto.getStudents() .stream()
		 * .map(stud->classRepository.findById(stud.getClassRoom().getId()))
		 * .collect(Collectors.toList());
		 */
		return new ResponseDto(classroom.get(),students,"Success","Data saved");
	}

	public ResponseDto getStudentDetails(int studentId) {
		List<Student> list = new ArrayList<>();
		Optional<Student> students = repository.findById(studentId);
		students.ifPresent(list::add);
		ClassRoom classRoom =students.get().getClassRoom();
		return new ResponseDto(classRoom,list,"Success","Student details for "+studentId);
	}
	public ResponseDto getStudentsDetailsbyClass(int classId) {
		List<Student> students = repository.findByClassRoomId(classId);
		ClassRoom classRoom =students.get(0).getClassRoom();
		return new ResponseDto(classRoom,students,"Success","All student details from "+classId);
	}

}
