package com.onetomany.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.onetomany.dto.ClassRoomRequestDto;
import com.onetomany.dto.ClassRoomResponseDto;
import com.onetomany.entity.ClassRoom;
import com.onetomany.repository.ClassRoomRepository;

@Service
public class ClassRoomService {
	
	@Autowired
	private ClassRoomRepository repository;
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE,
			readOnly = false)
	public ClassRoomResponseDto saveClassRoom(ClassRoomRequestDto classRequest) {
		ClassRoom classRoom = null;
		classRoom = repository.save(classRequest.getClassRoom());
		return new ClassRoomResponseDto(classRoom, "Success", "Data saved");
	}

	public ClassRoomResponseDto getClassRoomDetails(int classId) {
		Optional<ClassRoom> classRoom = repository.findById(classId);
		if(classRoom.isPresent())
			return new ClassRoomResponseDto(classRoom.get(),"Success",
					"Get data for class "+classId);
		return new ClassRoomResponseDto(null,"Failed",
				"Data not found for the class "+classId);
	}

	public ClassRoomResponseDto getAllClassRoomDetails() {
		List<ClassRoom> classRooms = repository.findAll();
		  if(!classRooms.isEmpty())
			  return new ClassRoomResponseDto("Success", "Get data for all class",classRooms);
		  
		  return new ClassRoomResponseDto(null,"Failed",
					"Class does not exist");
		 
	}

}
