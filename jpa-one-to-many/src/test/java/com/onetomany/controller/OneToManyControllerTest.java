package com.onetomany.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.onetomany.entity.ClassRoom;
import com.onetomany.repository.ClassRoomRepository;
import com.onetomany.service.ClassRoomService;

@RunWith(MockitoJUnitRunner.class)
public class OneToManyControllerTest {
	
	@InjectMocks
	private ClassRoomService service;
	
	@Mock
	private ClassRoomRepository repository;
	
	@Test
	public void getClassDetailsTest() {
		ClassRoom classRoom = new ClassRoom();
		classRoom.setId(1);
		classRoom.setName("Six");
		List<ClassRoom> classRooms = List.of(classRoom);
		when(repository.findAll())
		.thenReturn(classRooms);
		assertEquals(1, service.getAllClassRoomDetails().getClassRooms().size());
		//verify(service,times(1)).getAllClassRoomDetails();
	}

}
